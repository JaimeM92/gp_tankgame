﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inherited Class
public class PlayerData : TankData
{
    // If player collides with enemy shell, player takes damage
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyShell")
        {
            TakeDamage(damage);
        }
    }
}
