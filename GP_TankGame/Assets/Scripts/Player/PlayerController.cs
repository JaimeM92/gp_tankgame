﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Rate of Fire")]
    [Tooltip("Variables used to calculate the rate of fire when shooting")]
    public float ROF;
    public float ROFMax;
    private float ROFTimer;
   
    // Call to Components
    private TankMover tM;
    private TankShoot shoot;
    private AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        tM = GetComponent<TankMover>();
        shoot = GetComponent<TankShoot>();
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Player input to move tank forward and reverse
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
        {
            tM.MoveTank();
        }

        // Player input to rotate tank left and right
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            tM.RotateTank();
        }

        // Player shoot with Space key
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // FIre Shell if ROFTimer = 0
            if (ROFTimer <= 0)
            {
                shoot.Shoot();

                ROFTimer = ROFMax;
            }
        }

        // Decrement ROFTimer to fire again
        ROFTimer -= ROF * Time.deltaTime;
    }
}
