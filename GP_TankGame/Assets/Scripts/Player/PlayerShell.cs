﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inherited Class
public class PlayerShell : Shell
{
    // Player shell collides with enemy tank, destroy shell
    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }


}
