﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour
{
    // Variable to determine life of shell
    [SerializeField] private float shellLifeCounter;

    // Update is called once per frame
    public void Update()
    {
        // Shell life counter when fired
        if (shellLifeCounter > 0)
        {
            shellLifeCounter -= Time.deltaTime;
        }

        // Destroy shell when it reaches 0
        else
        {
            enabled = false;

            Destroy(gameObject);
        }
    }

    // Basic Function to destroy Shell object
    public virtual void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }
}
