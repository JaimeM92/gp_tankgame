﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Access transform of target object
    public Transform target;

    // Variable for placement of camera target object
    public Vector3 offsetX;

    // Variables for camera
    public float height;
    public float distance;
    public float camRotateSpeed;

    // Start is called before the first frame update
    void Start()
    {
        // Placement of camera
        offsetX = new Vector3(0, height, distance);
    }

    void LateUpdate()
    {
        // Rotate camera with Mouse on x-Axis
        offsetX = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * camRotateSpeed, Vector3.up) * offsetX;

        // Move camera along with target object
        transform.position = target.position + offsetX;
        transform.LookAt(target.position);
    }
}
