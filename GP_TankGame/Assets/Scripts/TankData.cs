﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Base Class
public class TankData : MonoBehaviour
{
    [Header("Tank Movement Variables")]
    [Tooltip("Variables to allow tank to move in seconds and drgrees")]
    public float forwardSpeed;
    public float reverseSpeed;
    public float rotateSpeed;

    [Header("Tank Health Variables")]
    [Tooltip("Variables to calculate total health of tank")]
    public float healthStart;
    public float currHealth;

    public Slider slider;
    public Image fillImage;
    public Color fullHealthColor = Color.green;
    public Color zeroHealthColor = Color.red;

    public float damage;

    private bool tankDestroyed;

    // Enable tank health and health bar at game
    public void OnEnable()
    {
        currHealth = healthStart;
        tankDestroyed = false;

        SetHealthUI();
    }

    // Function to add damage to tank health
    public void TakeDamage(float damageAmount)
    {
        currHealth -= damageAmount;

        // Call upon function for healthUI
        SetHealthUI();
        if(currHealth <= 0.0f && !tankDestroyed)
        {
            OnDeath();
        }
    }

    // Function for health UI 
    public void SetHealthUI()
    {
        // Start health value at current health value
        slider.value = currHealth;

        // Changes color slider depending on amount of health
        fillImage.color = Color.Lerp(zeroHealthColor, fullHealthColor, currHealth / healthStart);
    }

    // Deactivate Tank when destroyed
    public void OnDeath()
    {
        tankDestroyed = true;

        gameObject.SetActive(false);
    }
}
