﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inherited Class
public class AIShell : Shell
{
    // Enemy shell collides with player, destroy shell
    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
