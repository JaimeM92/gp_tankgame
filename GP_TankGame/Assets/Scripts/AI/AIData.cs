﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Inherited Class
public class AIData : TankData
{
    // If enemy tank collides with player shell, enemy tank takes damage
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerShell")
        {
            TakeDamage(damage);
        }
    }
}
