﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySight : MonoBehaviour
{
    [Header("Rate of Fire")]
    [Tooltip("Variables to determine rate of fire when firing for AI")]
    public float ROF;
    public float ROFMax;
    private float ROFTimer;
    [Space(5)]

    // Call to Components
    public NavMeshAgent agent;
    private AIData data;
    private TankShoot shoot;

    // States for AI
    public enum State
    {
        PATROL,
        CHASE,
        RETREAT
    }

    public State state;

    // Variable to determine if tank is still in game
    private bool alive;

    [Header("Waypoints")]
    [Tooltip("List of waypoints for AI to patrol")]
    public GameObject[] waypoints;
    private int waypointInd;

    // Set Target Object to follow when triggered
    public GameObject target;

    // Variables for sight
    public float heightMultiplier;
    public float sightDistance;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        data = GetComponent<AIData>();
        shoot = GetComponent<TankShoot>();

        agent.updatePosition = true;
        agent.updateRotation = true;

        // AI locates and travels to random waypoints
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
        waypointInd = Random.Range(0, waypoints.Length);

        // AI starts in Patrol state
        state = EnemySight.State.PATROL;

        // Ai starts off in game
        alive = true;

        heightMultiplier = 1.36f;

        StartCoroutine("FSM");
    }

    // Switch States 
    IEnumerator FSM()
    {
        while (alive)
        {
            switch (state)
            {
                case State.PATROL:
                    Patrol();
                    break;

                case State.CHASE:
                    Chase();
                    break;

                case State.RETREAT:
                    Retreat();
                    break;
            }

            yield return null;

        }
    }

    // Patrol State
    void Patrol()
    {
        // Overwrite Nav Mesh Speed with AI data speed
        agent.speed = data.forwardSpeed;

        if (Vector3.Distance(this.transform.position, waypoints[waypointInd].transform.position) >= 2)
        {
            // Travel to waypoint
            agent.SetDestination(waypoints[waypointInd].transform.position);
            RaycastHit hit;
            //Draw Raycast for LOS
            Debug.DrawRay(transform.position + Vector3.up * heightMultiplier, transform.forward * sightDistance, Color.red);
            Debug.DrawRay(transform.position + Vector3.up * heightMultiplier, (transform.forward + transform.right).normalized * sightDistance, Color.red);
            Debug.DrawRay(transform.position + Vector3.up * heightMultiplier, (transform.forward - transform.right).normalized * sightDistance, Color.red);

            // If forward Raycast hits player
            if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, transform.forward, out hit, sightDistance))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    state = EnemySight.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }

            // If right Raycast hits player
            if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, (transform.forward + transform.right).normalized, out hit, sightDistance))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    state = EnemySight.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }

            // If left Raycast hits player
            if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, (transform.forward - transform.right).normalized, out hit, sightDistance))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    state = EnemySight.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }

            // Switch State if enemy tank health drops below 50%
            if (data.currHealth < data.healthStart / 2)
            {
                state = EnemySight.State.RETREAT;
            }
        }
        
        else if (Vector3.Distance(this.transform.position, waypoints[waypointInd].transform.position) <= 2)
        {
            waypointInd = Random.Range(0, waypoints.Length);
        }
    }

    // Chase State
    void Chase()
    {
        // Overwrite Nav Mesh Speed with AI data speed
        agent.speed = data.forwardSpeed;
        
        // Player becomes destination for enemy tank
        agent.SetDestination(target.transform.position);

        // Shoot at player
        if (ROFTimer <= 0)
        {
            shoot.Shoot();

            ROFTimer = ROFMax;
        }

        // Cool down for rate of fire
        ROFTimer -= ROF * Time.deltaTime;

        // Switch State if enemy tank health drops below 50%
        if (data.currHealth < data.healthStart / 2)
        {
            state = EnemySight.State.RETREAT;
        }
    }

    void Retreat()
    {
        // Overwrite Nav Mesh Speed with AI data speed
        agent.speed = data.forwardSpeed;
 
        // Enemy tank will move away from target 
        agent.SetDestination(transform.position + target.transform.position);
      
    }

    // If player triggers sphere collider switch to Chase State
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            state = EnemySight.State.CHASE;
            target = other.gameObject;
        }
    }
}

