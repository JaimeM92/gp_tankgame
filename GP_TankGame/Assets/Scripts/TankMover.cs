﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Base Class
public class TankMover : MonoBehaviour
{
    // Access TankData script
    private PlayerData tD;

    // Start is called before the first frame update
    void Start()
    {
        tD = GetComponent<PlayerData>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Function to move tank forward and reverse
    public void MoveTank()
    {
        // Access CharacterController of object
        CharacterController controller = GetComponent<CharacterController>();

        // Move tank forward
        Vector3 move = transform.TransformDirection(Vector3.forward);
        float curSpeed = tD.forwardSpeed * Input.GetAxis("Vertical");

        // Player input moves tank in reverse
        if (Input.GetKey(KeyCode.S))
        {
            // Move tank in reverse
            move = transform.TransformDirection(Vector3.back);
            curSpeed = tD.reverseSpeed * Input.GetAxis("Vertical");
        }

        // Move tank 
        controller.SimpleMove(move * curSpeed);
    }

    // Function to rotate tank
    public void RotateTank()
    {
        transform.Rotate(0, Input.GetAxis("Horizontal") * tD.rotateSpeed * Time.deltaTime, 0);
    }
}
