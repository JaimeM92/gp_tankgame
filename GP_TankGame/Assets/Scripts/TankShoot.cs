﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShoot : MonoBehaviour
{
    // Variable for the speed the shell travels
    [Header("Variable for speed")]
    [Tooltip("Determines the speed at which shell will travel")]
    public float shellSpeed;

    // Access components for shell and fireplacement 
    public Rigidbody shell;
    public Transform fireStart;

    // Access transform for tank barrel
    private Transform turret;

    // Start is called before the first frame update
    void Start()
    {
        turret = fireStart.parent;
    }

    // Shoot the shell from the fireStart position
    public void Shoot()
    {
       Rigidbody rbShell = Instantiate(shell, fireStart.position, turret.rotation);
       rbShell.velocity = shellSpeed * turret.forward; 
    }
}
